/*
 * We truck in a state tree that looks like this:
 *    {
 *      visibilityFilter: SHOW_ALL,
 *      todos: [
 *        {text: "blah blah", completed: false},
 *        {text: "asd sdf", completed: true},
 *      ]
 *    }
 *
 */
import { combineReducers } from 'redux';
import { ADD_TODO, COMPLETE_TODO, SET_VISIBILITY_FILTER, VisibilityFilters} from './actions.js';

const { SHOW_ALL } = VisibilityFilters;

function visibilityFilter(state = SHOW_ALL, action){
  switch (action.type){
    case SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
}

function todos(state = [], action){
  switch (action.type){
    case ADD_TODO:
      return [...state, {
        text: action.text,
        completed: false
      }];
    case COMPLETE_TODO:
      return [
        ...state.slice(0, action.index),
        Object.assign({}, state[action.index], {completed: true}),
        ...state.slice(action.index + 1)
      ];
    default:
      return state;
  }
}

// function todoApp(state, action){
//   return {
//     visibilityFilter: visibilityFilter(state, action),
//     todos: todos(state, action)
//   };
// }

export const todoApp = combineReducers({
  visibilityFilter,
  todos
});
// export const todoApp = 123;


export default todoApp;
