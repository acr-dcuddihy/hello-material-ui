import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { addTodo, completeTodo} from './actions'
import { setVisibilityFilter, VisibilityFilters} from './actions'
import { AddTodo } from './AddTodo';
import { TodoList } from './TodoList';
import { Footer } from './Footer';
import './App.css';
// === Material UI Demo: ===
// import injectTapEventPlugin from 'react-tap-event-plugin';
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import MyAwesomeReactComponent from './MyAwesomeReactComponent';
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
//  injectTapEventPlugin();
// =========================

class App extends Component {
  render() {
    const { dispatch, visibleTodos, visibilityFilter } = this.props;
    return (
      <div className="App">
        <AddTodo onAddClick={text => dispatch(addTodo(text))}/>
        <TodoList
          todos={visibleTodos}
          onTodoClick={index => dispatch(completeTodo(index))}/>
        <Footer filter={visibilityFilter}
                onFilterChange={fltr=>dispatch(setVisibilityFilter(fltr))}/>
      </div>

      /* We'll bring back this awesome app as a routing exercise.
      <div className="App">
        <MuiThemeProvider>
          <MyAwesomeReactComponent />
        </MuiThemeProvider>
      </div>
      */
    );
  }
}
App.propTypes = {
  visibleTodos: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired
  })),
  visibilityFilter: PropTypes.oneOf([
                                      'SHOW_ALL',
                                      'SHOW_COMPLETED',
                                      'SHOW_ACTIVE'
                                    ]).isRequired
};

function selectTodos(todos, filter){
  switch(filter){
    case VisibilityFilters.SHOW_ALL:
      return todos;
    case VisibilityFilters.SHOW_COMPLETED:
      return todos.filter(todo => todo.completed);
    case VisibilityFilters.SHOW_ACTIVE:
      return todos.filter(todo => !todo.completed);
    default:
      return todos;
  }
}

function select(state){
  return {
    visibleTodos: selectTodos(state.todos, state.visibilityFilter),
    visibilityFilter: state.visibilityFilter
  }
}


export default connect(select)(App);
