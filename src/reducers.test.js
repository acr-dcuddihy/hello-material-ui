import { VisibilityFilters } from './actions.js';
import { setVisibilityFilter } from './actions.js';
import { addTodo } from './actions.js';
import { todoApp } from './reducers';

it('updates state correctly on set-visibility-filter action.', () => {
  const axn = setVisibilityFilter(VisibilityFilters.SHOW_ALL);
  // expect(todoApp).toBe('asd');
  const dummyTodo = {text:'asd', completed:false}
  const newState = todoApp({todos:[dummyTodo]}, axn);
  expect(newState.visibilityFilter).toBe(VisibilityFilters.SHOW_ALL);
  expect(newState.todos).toEqual([dummyTodo]);
});

it('adds a todo to state on add-todo action.', () => {
  const axn = addTodo('something');
  const dummyTodo = {text:'asd', completed:false};
  const expTodo = {'text':'something', completed:false};
  const newState = todoApp({todos:[dummyTodo]}, axn);
  expect(newState.visibilityFilter).toBe(VisibilityFilters.SHOW_ALL);
  expect(newState.todos).toEqual([dummyTodo, expTodo]);
});
