/*
    Reducers to manipulate state of the following shape, as per actions.


    {
      selectedReddit: 'frontend',
      postsByReddit: {
        frontend: {
          isFetching: true,
          didInvalidate: false,
          items: []
        },
        reactjs: {
          isFetching: false,
          didInvalidate: false,
          lastUpdated: 1439478405547,
          items: [
            {
              id: 42,
              title: 'Confusion about Flux and Relay',
            },
            {
              id: 500,
              title: 'Creating a Simple Application Using React JS and ...'
            }
          ]
        }
      }


*/
import { combineReducers } from 'redux';
import {
  SELECT_REDDIT, INVALIDATE_REDDIT,
  REQUEST_POSTS, RECEIVE_POSTS
} from './reddit.actions';


function selectedReddit(state = 'reactjs', action){
  switch (action.type) {
    case SELECT_REDDIT:
      return action.reddit;
    default:
      return state;
  }
}

function posts(state = {
                isFetching: false,
                didInvalidate: false,
                items: []
              }, action) {

  switch (action.type) {
    case INVALIDATE_REDDIT:
      return Object.assign({}, state, {didInvalidate: true});
    case REQUEST_POSTS:
      return Object.assign({}, state, {
                                        isFetching: true,
                                        didInvalidate: false
                                      });
    case RECEIVE_POSTS:
      return Object.assign({}, state, {
                                        isFetching: false,
                                        didInvalidate: false,
                                        items: action.posts,
                                        lastUpdated: action.receivedAt
                                      });
    default:
      return state;
  }
}


function postsByReddit(state = {}, action) {
  switch (action.type) {
    case INVALIDATE_REDDIT:
    case RECEIVE_POSTS:
    case REQUEST_POSTS:
      return Object.assign({}, state, {
        // [computed property]
        // ..equiv:
        // nextState[action.reddit] = posts(state[action.reddit], action);
        [action.reddit]: posts(state[action.reddit], action)
      });
    default:
      return state;
  }
}

export const rootReducer = combineReducers({
  postsByReddit,
  selectedReddit
});
