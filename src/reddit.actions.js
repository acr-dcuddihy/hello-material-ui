import fetch from 'isomorphic-fetch';

export const SELECT_REDDIT = 'SELECT_REDDIT';

export function selectReddit(reddit) {
  return {
    type: SELECT_REDDIT,
    reddit
  };
}

export const INVALIDATE_REDDIT = 'INVALIDATE_REDDIT';

export function invalidateReddit(reddit) {
  return {
    type: INVALIDATE_REDDIT,
    reddit
  };
}

export const REQUEST_POSTS = 'REQUEST_POSTS';

export function requestPosts(reddit){
  return {
    type: REQUEST_POSTS,
    reddit
  };
}

export const RECEIVE_POSTS = 'RECEIVE_POSTS';

export function receivePosts(reddit, json){
  return {
    type: RECEIVE_POSTS,
    reddit,
    posts: json.data.children.map(child =>child.data),
    receivedAt: Date.now()
  };
}

export function fetchPosts(reddit){
  // Thunk passes dispatch method as an argument.
  return function (dispatch) {
    // Dispatch: The app state is updated to inform the API call is starting.
    dispatch(requestPosts(reddit));
    // function called by thunk can return a value that is passed on as the
    // return value of the dispatch method.
    // ..in our case, we return a promise to wait for.
    return fetch('http://www.redit.com/r/${reddit}.json')
      .then(response => response.json())
      .then(json => dispatch(receivePosts(reddit, json)));

  }
}
