export const RQ_ASOFS='RQ_ASOFS';

export function rq_asofs(){
  return {
    type: RQ_ASOFS
  };
}

export const RESP_ASOFS='RESP_ASOFS';

export function resp_asofs(asofs){
  return {
    type: RESP_ASOFS,
    asofs
  };
}

export const CHOOSE_ASOF='CHOOSE_ASOF';

export function choose_asof(asof){
  return {
    type: CHOOSE_ASOF,
    asof
  };
}

export const RQ_BOOKS='RQ_BOOKS';

export function rq_books(asof){
  return {
    type: RQ_BOOKS,
    asof
  };
}

export const RESP_BOOKS='RESP_BOOKS';

export function resp_books(books){
  return {
    type: RESP_BOOKS,
    books
  }
}

export const RQ_ISSUERS='RQ_ISSUERS';

export function rq_issuers(asof, book){
  return {
    type: RQ_ISSUERS,
    asof,
    book
  };
}

export const RESP_ISSUERS='RESP_ISSUERS';

export function resp_issuers(issuers){
  return {
    type: RESP_ISSUERS,
    issuers
  };
}
