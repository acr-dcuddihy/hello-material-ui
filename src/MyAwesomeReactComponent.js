import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import DemoChart from './DemoChart';

// Note: Material-UI unfortunately doesn't support React CSS imports.
const material_css_paper = {
    height: 200,
    width: 200,
    margin: 20,
    fontFamily: 'roboto, sans-serif',
    textAlign: 'center',
    backgroundColor: '#FFFFA5',
    display: 'inline-block',
  };

  const material_css_textfield = {
      margin: 20,
      fontFamily: 'roboto, sans-serif',
      display: 'inline-block'
  };

export default class MyAwesomeReactComponent extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      open: true,
      displayMsg: 'A bar chart.',
      faveNo: 123,
    };
  }

  handleToggle = () => this.setState({open: !this.state.open});

  onRecalc = () => {
    this.chart.recalculate();
  }

  closeMenu = () => {
    this.setState({open: false});
  }

  onChartHover = (cmd, details) => {
    let msg = 'A bar chart.';
    if(details){
      msg = "f(" + details.x + ") = " + details.y;
    }
    this.setState({displayMsg:msg})
  }

  onNewFavNumber = (event, newValue) => {
    this.setState({faveNo: newValue});
  }

  render() {
    const button_title = this.state.open ? 'Hide Menu' : 'Show Menu';
    return (
      <div className="my-awesome-react-component">
          <RaisedButton label={button_title} onTouchTap={this.handleToggle}/>
          <RaisedButton label="Recalculate" onTouchTap={this.onRecalc}/>
          <Drawer open={this.state.open}>
              <MenuItem onTouchTap={this.onRecalc}>Recalculate</MenuItem>
              <MenuItem onTouchTap={this.closeMenu}>Close</MenuItem>
              <TextField
                style={material_css_textfield}
                hintText="Favorite Number"
                floatingLabelText="Your Favorite Number"
                defaultValue={this.state.faveNo}
                onChange={this.onNewFavNumber} />
          </Drawer>
          <DemoChart ref={(elem) => { this.chart = elem;}}
                     onSignalHover={ this.onChartHover } />
          <Paper zDepth={5} className="postit" style={material_css_paper}>
            <h3>Active Post-It:</h3>
            <p>{this.state.displayMsg}</p>
            <p>{this.state.faveNo}</p>
          </Paper>
    </div>
    )
  }
}
