/*
Datashape for pfquery:

  {
    asof: {
      selected: '20170324',
      available: ['latest', '20170324', ...],
      querying: false
    },
    book: {
      selected: 'RG',
      available: ['all', 'RG', 'MS', ...],
      querying: false
    },
    riskIssuer: {
      selected: 'all',
      available: ['all', 'ED', 'EWZ', ...],
      querying: false
    },
  }
*/

/* utility fn. to return a selection if available and return
   first available if given selection is not available.
*/
function newSel(sel, avail){
    if (sel in avail){
      return sel;
    }else{
      return avail[0];
    }
}

function update(orig, update){
  return Object.assign({}, orig, update);
}


export function asof(state={selected:'all',
                            available:[],
                            querying:false}, action){
  switch(action.type){
    case RQ_ASOFS:
      return update(state, {querying:true});
    case RESP_ASOFS:
      newSel = calc_new_sel(selected, action.asofs)
      return update(state, {selected:newSel,
                            available: action.asofs,
                            querying: false})
    case CHOOSE_ASOF:
      return update(state, {selected:action.asof});
    default:
      return state;
  }
}

export function book(state={selected:'all',
                            available:[],
                            querying:false}, action){
  switch(action.type){
    case RQ_BOOKS:
      return update(state, {querying:true});
    case RESP_BOOKS:
      newSel = calc_new_sel(selected, action.books)
      return update(state, {selected:newSel,
                            available: action.books,
                            querying: false})
    case CHOOSE_BOOK:
      return update(state, {selected:action.books});
    default:
      return state;
  }
}

export function issuer(state={selected:'all',
                              available:[],
                              querying:false}, action){
  switch(action.type){
    case RQ_ISSUERS:
      return update(state, {querying:true});
    case RESP_ISSUERS:
      newSel = calc_new_sel(selected, action.issuers)
      return update(state, {selected:newSel,
                            available: action.issuers,
                            querying: false})
    case CHOOSE_ISSUER:
      return update(state, {selected:action.issuer});
    default:
      return state;
  }
}
