import {addTodo, ADD_TODO} from './actions.js';
import {SET_VISIBILITY_FILTER} from './actions.js';
import {setVisibilityFilter} from './actions.js';
import {VisibilityFilters} from './actions.js';


it('creates a populated addTodo action', () => {
  const axn = addTodo('blah blah');
  expect(axn.type).toBe(ADD_TODO);
  expect(axn.text).toBe('blah blah');
});

it('creates a populated setVisibilityFilter action', () => {
  const axn = setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED);
  expect(axn.type).toBe(SET_VISIBILITY_FILTER);
  expect(axn.filter).toBe(VisibilityFilters.SHOW_COMPLETED);
});
