import React from 'react';
import {createClassFromSpec} from 'react-vega';
import {barSpec} from './DemoChart.vega';

const BarChart = createClassFromSpec(barSpec);

export default class DemoChart extends React.Component{
  constructor(){
    super();
    this.state = {barData: this.calcBarData()};
  }
  render(){
    return (
      <BarChart data={this.state.barData}
                onSignalHover={this.props.onSignalHover}/>
    )
  }
  logSpec(){
    console.log("barSpec=" + JSON.stringify(barSpec));
  }
  handleHover(...args){
    console.log(args);
  }
  recalculate(){
    this.setState({barData: this.calcBarData()});
  }
  calcBarData(){
    return {
      table: Array(20).fill(0).map(
                  (v,i) => { return {"x":i+1,
                                     "y":Math.round(Math.random()*100)}
                           })
      }
  }
}
