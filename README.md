# Hello Material UI
This is my test-project for integrating the various UI components that I'd like
to use to deliver apps to the business. This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) and uses
the following client-side technologies:

* ReactJS
* material-ui
* vega and react-vega


## Install Prerequisites
I'm targeting an Ubuntu 14.04 environment.  To get the full-featured Install
of react-vega, you'll want to run this first:

    sudo apt-get install libjpeg-dev libgif-dev

(Though why it's compiling C code on install is a mystery to me!)
